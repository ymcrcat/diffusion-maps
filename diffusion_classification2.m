function class = diffusion_classification2(xtrain, ytrain, xtest, diffusion_params)
    % Classify new points in the 'xtest' set using geometric harmonics extension
    % of a diffusion map calculated on the 'xtrain' set.
    % Each column in xtest represents a point to be classified.
    % Each column in xtrain contains a training sample.
    % ytrain - column vector.
    
    global T; % diffusion steps
        
    data = [xtrain xtest];
    [phi, lambda] = diffusion_map(data, diffusion_params);
    points = embedding(phi, lambda, T);
    
    train_points = points(1:size(xtrain, 2), :);
    test_points = points(end-size(xtest, 2)+1:end, :);
    class = knnclassify(test_points, train_points, ytrain, 10);
end