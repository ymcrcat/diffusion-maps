function points = embedding(phi, lambda, T)
    % Embedding in Diffusion Space
    % Phi - eigenvectors
    % Lambda - eigenvalues
    % T - number of diffusion steps
    points = phi * diag(lambda .^ T);
end