function [Phi, Lambda, W] = diffusion_map2(X, alpha)

global epsilon;

D = L2_distance(X,X,1);
W = exp(-(D/epsilon).^2);
p = sum(W);
p = p(:);
K1 = W./((p*p').^alpha);
v = sqrt(sum(K1));
v = v(:);
A = K1./(v*v');
[Phi,S,V] = svd(A,0);   %Full version.
Phi = Phi./(Phi(:,1)*ones(1,size(Phi,1)));
Lambda = diag(S);
end

function d = L2_distance(a,b,df)
if (size(a,1) == 1)
  a = [a; zeros(1,size(a,2))]; 
  b = [b; zeros(1,size(b,2))]; 
end;
aa=sum(a.*a); bb=sum(b.*b); ab=a'*b; 
d = sqrt(repmat(aa',[1 size(bb,2)]) + repmat(bb,[size(aa,2) 1]) - 2*ab);
d = real(d); 
if (df==1)
  d = d.*(1-eye(size(d)));
end;
end