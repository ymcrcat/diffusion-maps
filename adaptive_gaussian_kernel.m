function K = adaptive_gaussian_kernel(A, initial_scale)
    % A - samples matrix, each sample is a column in A
    
    a = L2_distance(A, A);
    w = exp(-a / initial_scale); % w is a row vector of weights for each sample
    w = sum(w);
    
    [wi, wj] = meshgrid(w, w);
    Omega = sqrt(wi .* wj); % Omega is symmetric and non-negative
    assert(all(all(Omega == Omega')));
    a = a' * a;
    K = exp(-a ./ Omega); % K is symmetric and non-negative
end
