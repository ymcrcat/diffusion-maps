function diff_x = diffusion_extension(phi, lambda, sample_data, x, W, epsilon_vector)
    % Implementation of Geometric Harmonics based on the article:
    % "Data Fusion and Multicue Data Matching by Diffusion Maps", Lafon et.
    % al.
    
    N = size(sample_data, 2);
    c = zeros(1, N);
    for k=1:N
        c(k) = kernel(x, sample_data(:, k), epsilon_vector);
    end;
    
%     global epsilon;
%     c = L2_distance(x, sample_data);
%     c = exp( -c.^2 ./ epsilon );
    
    c = c./ sum(c);
    
    diff_x = c * phi * diag(1./lambda);
end