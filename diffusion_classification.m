function class = diffusion_classification(xtrain, ytrain, xtest, diffusion_params)
    % Classify new points in the 'xtest' set using geometric harmonics extension
    % of a diffusion map calculated on the 'xtrain' set.
    % Each column in xtest represents a point to be classified.
    % Each column in xtrain contains a training sample.
    % ytrain - column vector.
    
    global T; % diffusion steps
        
    [phi, lambda, W, D, epsilon_vector] = diffusion_map(xtrain, diffusion_params);
    points = embedding(phi, lambda, T);
    
    diff_x = gh_embedding(phi, lambda, xtrain, xtest, diffusion_params.diffusion_dimension, W, epsilon_vector);
    
%     new_points = [lambda(2)^T*diff_x(:,2) lambda(3)^T*diff_x(:,3) lambda(4)^T*diff_x(:,4)];
%     new_points = [lambda(2)^T*diff_x(:,2) lambda(3)^T*diff_x(:,3)];
%     class = classify(new_points, points, ytrain, 'linear');
    new_points = diff_x * diag(lambda.^T);
    class = knnclassify(new_points, points, ytrain, 10);
end