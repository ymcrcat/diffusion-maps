function diff_x = gh_embedding(phi, lambda, xtrain, xtest, diffusion_dimension, W, epsilon_vector)
    % Embedding of new samples via Geometric Harmonics
    NEW_POINTS_NUM = size(xtest, 2);
    
    diff_x = zeros(NEW_POINTS_NUM, diffusion_dimension);
    for k=1:NEW_POINTS_NUM
        diff_x(k, :) = diffusion_extension(phi, lambda, xtrain, xtest(:, k), W, epsilon_vector);
    end;
end