function class = diffusion_clustering(data, k)
    % data - Data to cluster. Eeach column is a sample.
    % k - number of clusters.
    
    global diffusion_dimension;
    global T;
    
    [phi, lambda] = diffusion_map(data);
    
    % coordinates in diffusion spaces
    points = embedding(phi(:, 2:diffusion_dimension ) , lambda(2:diffusion_dimension), T);
    class = kmeans(points, k);
end