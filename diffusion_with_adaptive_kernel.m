function [Phi, Lambda, P] = diffusion_with_adaptive_kernel(x, params)
    initial_scale = params.epsilon;
    t = 2;
    
    P = affinity_matrix(x, initial_scale, t);
    
    eigs_opt = struct('disp', 0);
    [EVec, EVal] = eigs(P, params.diffusion_dimension, 'lm', eigs_opt); % return largest eigenvalues
    Lambda = diag(EVal);
    [Lambda, ind] = sort(Lambda, 'descend');

    % Sort eigenvectors according to the magnitude of the correspoing
    % eigenvalues.
    Phi = EVec(:, ind);
end