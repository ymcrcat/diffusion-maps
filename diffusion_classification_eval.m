function [correct_rate, correct_rate_beta_zero, c_range] = ...
    diffusion_classification_eval(xtrain, ytrain, xtest, ytest)
    c_range = logspace(log10(5), log10(50), 10);
    correct_rate = zeros(length(c_range), 1);
    correct_rate_beta_zero = zeros(length(c_range), 1);
    
    diff_params.symmetric = false;
    
    for k = 1:length(c_range)
        diff_params.C = c_range(k);
        
        try
            diff_params.beta = 1;   
            class = diffusion_classification(xtrain, ytrain, xtest, diff_params);
            cp = classperf(ytest, class);
            correct_rate(k) = cp.CorrectRate;
        catch ME
            ME.stack(1)
        end;
        
        try
            diff_params.beta = 0;
            class = diffusion_classification(xtrain, ytrain, xtest, diff_params);
            cp = classperf(ytest, class);
            correct_rate_beta_zero(k) = cp.CorrectRate;
        catch ME
            ME.stack(1)
        end;
    end;
    plot(c_range, correct_rate*100, c_range, correct_rate_beta_zero*100);
    ylabel('Correct classification percentage');
    xlabel('C');
    legend('\beta=1', '\beta=0');
end