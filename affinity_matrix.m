function P = affinity_matrix(A, initial_scale, t)
    % Calculation of an adaptive gaussian kernel repeated 2 times for
    % better representation of the local geometry
    K = adaptive_gaussian_kernel(A, initial_scale);
    
    % normalization of the Gaussian kernel into a Markov transition matrix
    % Graph Laplacian matrix definition is used for the normalization.
    % Another option is using the Laplace-Beltrami matrix definition.
    D = sum(K, 2);
    [di, dj] = meshgrid(D, D);
    D = sqrt(di .* dj);
   
    % Step 5
    P = K ./ D;
    P = P ^ t;
end