function varargout = epsilon_estimation(data)
    % Kernel bandwidth parameter selection implemented according to the
    % method presented in the article: "Graph laplacian tomography from
    % unknown random projections" by A. Singer.
    
    % If no output argument is specified the function will plot a graph of
    % the volume log as a function of epsilon. If an output argument is
    % specified it will return the value.
    
    e = logspace(-3, 3, 20);
    Le = zeros(1, length(e)); % Singer Measure (SM)
    % global epsilon
    
%     global kNN;
    
    for k = 1:length(e)
        epsilon = e(k);
        W = L2_distance(data, data);
        % calculate the exponent which gives us the gaussian kernel
        W = exp(-W.^2  ./ epsilon );
        
%         cd ../nn
%         W = calc_distance_matrix(data, kNN, epsilon);
%         cd ../diffusion_maps
        Le(k) = sum(sum(W)); % calculate volume of W
    end;
    
    % Find linear region by finding the minimum of the 2nd derivative
    Le_1st_deriv = Le(2:end) - Le(1:end-1);
    Le_2nd_deriv = Le(3:end) - 2*Le(2:end-1) + Le(1:end-2);
    [minval, ind] = min(Le_2nd_deriv);
    [maxval, ind2] = max(Le_1st_deriv);
    if ind == 1
        epsi = e(1);
        yval = Le(1);
    else
        epsi = e(ind - 1);
        yval = Le(ind - 1);
    end;
    
    % Find linear region by finding the maximum of the 1st derivative
    epsi2 = e(ind2);
    yval2 = Le(ind2);
    
    if (nargout == 0)
        figure;
        plot(log10(e), Le);
        xlabel('log_{10}\epsilon');
        ylabel('L(\epsilon)');
        hold on;
        plot(log10(epsi), yval, 'ro');
        plot(log10(epsi2), yval2, 'go');
        legend('L(\epsilon)', 'Maximum of 1st derivative', 'Minimum of 2nd derivative');
        title('\epsilon estimation');
        hold off;
        
        display(['epsilon=', num2str(epsi2)]);
    end;
    
    if nargout > 0
        varargout{1} = epsi2;
    end
end