clear;
close all;

N = 30;

% Original manifold
[swissroll, labels, t] = generate_data('swiss', N^2);
x = swissroll(:,1);
y = swissroll(:,2);
z = swissroll(:,3);
t = t(:,1);
[t, ind] = sort(t);
x = x(ind, :);
y = y(ind,:);
y(y > 20) = 20;
y(y < 12) = 12;
z = z(ind,:);
x = reshape(x, [N N]);
y = reshape(y, [N N]);
z = reshape(z, [N N]);
t = reshape(t, [N N]);
subplot(1, 2, 1);
surf(x, y, z, t, 'Edgecolor', 'none');
shading interp;
grid off;
axis('off');

% Sampled manifold
[swissroll, labels, t] = generate_data('swiss', N*100);
x = swissroll(:,1);
y = swissroll(:,2);
z = swissroll(:,3);
t = t(:,1);
subplot(1, 2, 2);
scatter3(x, y, z, 10, t, 'filled')
grid off;
axis('off');

params = struct();
params.symmetric = false;
params.diffusion_dimension = 4;
params.C = 1;
params.epsilon = 1;
T = 2;
[phi, lambda] = diffusion_map(swissroll', params);

dx = lambda(2)^T*phi(:, 2);
dy = lambda(3)^T*phi(:, 3);
dz = lambda(4)^T*phi(:, 4);

figure;
subplot(121);
scatter(dx, dy, 10, dx, 'filled');
grid off;
axis('off');
% title('Diffusion embedding using first two nontrivial eigenvectors');

h = subplot(122);
plot_clustered_data([x y z], dx, h);
% title('Manifold colored according to first nontrivial eigenvector');
grid off;
axis('off');