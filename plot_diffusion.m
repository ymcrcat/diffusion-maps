function plot_diffusion(phi, lambda, T, labels)
    % Plot embedding of diffusion map:
    % plot_diffusion(phi, lambda, T, labels)
    x = lambda(2)^T*phi(:, 2);
    y = lambda(3)^T*phi(:, 3);
    z = lambda(4)^T*phi(:, 4);
    
    plot_clustered_data([x y z], labels);
    title('Diffusion Embedding');
    xlabel('\psi_2'); ylabel('\psi_3'); zlabel('\psi_4');
end