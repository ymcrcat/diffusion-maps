function epsilon = max_min_measure(data)
    % Max-min measure method for choosing the kernel bandwidth parameter.
    % Implemented according to "Audio-Visual Group Recognition using
    % Diffusion Maps" by Y. Keller et. al.
    C = 2;
    
    N = size(data, 2);
    
    W = L2_distance(data, data);
    ind = (1:N).^2;
    W(ind) = NaN;
    
    H = nanmin(W);
    epsilon = C * max(H);
end