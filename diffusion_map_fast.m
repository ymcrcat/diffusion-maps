%% Transform data into diffusion space
function [Phi, Lambda, W, D] = diffusion_map_fast(x, C, beta)

global diffusion_dimension;
% global epsilon;

USE_GRAPH_LAPLACIAN = false;

% N - the dimension of the original data
N = size(x, 2);
 
W = L2_distance(x, x);
W = exp(-W.^2  ./ C );
W = W - (1-beta) * eye(N);

% normalize the diffusion matrix L
% to obtain the random-walk matrix M
D = diag(sum(W, 2));

eigs_opt = struct('disp', 0);

if USE_GRAPH_LAPLACIAN
    L = D - W; % Graph Laplacian matrix
    Dinv = D^(-1/2);

    % The following normalization should be more suitable for non-uniform
    % distribution
    M = Dinv * L * Dinv; % symmetric normalized graph-laplacian
%     M = D \ L; % random-walk normalized graph-laplacian
    
    % return smallest eigenvalues
    [EVec, EVal] = eigs(M, diffusion_dimension, 'sm', eigs_opt); 
    Lambda = diag(EVal);
    [Lambda, ind] = sort(Lambda, 'ascend');
else
    M = D \ W; %like inv(D)*L
    
    [EVec, EVal] = eigs(M, diffusion_dimension, 'lm', eigs_opt); % return largest eigenvalues
    Lambda = diag(EVal);
    [Lambda, ind] = sort(Lambda, 'descend');
end;

% Sort eigenvectors according to the magnitude of the correspoing
% eigenvalues.
Phi = EVec(:, ind);
end
