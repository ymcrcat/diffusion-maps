%% Transform data into diffusion space
function [Phi, Lambda, W, D, epsilon_vector] = diffusion_map(x, diffusion_params)
% diffusion_params - C, beta, symmetric

% N - the dimension of the original data
N = size(x, 2);

USE_DRTOOLBOX = false;

if ~USE_DRTOOLBOX
    epsilon_vector = var(x, 0, 2) * diffusion_params.C;
    W = zeros(N); % Weight matrix, also called W
    TOTAL_ITERATIONS = (N-1)^2;
    progressbar;
    for n=2:N
        for m=1:(n-1)
            W(n, m) = kernel(x(:,n), x(:, m), epsilon_vector);
            progressbar(((n-2)*(n-1)+m)/TOTAL_ITERATIONS);
        end;
    end;
    % W is a symmetric matrix and we filled half of it
    W = W + W.' + diffusion_params.beta * eye(N);
else
    W = L2_distance(x, x);
    W = exp(-W.^2 / diffusion_params.epsilon );
end

% cd ../nn
% global kNN;
% W = calc_distance_matrix(x, kNN, epsilon);
% cd ../diffusion_maps

% normalize the diffusion matrix L
% to obtain the random-walk matrix M
D = diag(sum(W, 2));

eigs_opt = struct('disp', 0);

if diffusion_params.symmetric
    Dinv = D^(-1/2);
    
    % The following normalization should be more suitable for non-uniform
    % distribution
    M = Dinv * W * Dinv;
else
    M = D \ W; %like inv(D)*L
end;

% return largest eigenvalues
[EVec, EVal] = eigs(M, diffusion_params.diffusion_dimension, 'lm', eigs_opt); 
Lambda = diag(EVal);
[Lambda, ind] = sort(Lambda, 'descend');

% Sort eigenvectors according to the magnitude of the correspoing
% eigenvalues.
Phi = EVec(:, ind);
end
