%% Gaussian kernel function
function L = kernel(x, y, epsilon_vector)
    global epsilon;
    if nargin() < 3
        L = exp( -norm(x - y)^2 / epsilon);
    else
        % a different epsilon is used for different features
        d = (x - y);
        L = exp(- d' * diag(1./epsilon_vector) * d);
    end;
end