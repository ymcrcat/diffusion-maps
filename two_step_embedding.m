% This script performs embedding of each feature separately and constructs
% a new feature matrix based on the embeddings of each feature. The new
% feature matrix is then again processed with a diffusion map.

configure;

global epsilon;
global T;

NUM_OF_FEATURES = size(feature_matrix, 1);

embedded_features = [];

for k = 1:NUM_OF_FEATURES
    feature = feature_matrix(k, :);
    epsilon = epsilon_estimation(feature);
    [phi, lambda] = diffusion_map2(feature, 1);
    points = embedding(phi(:, 2), lambda(2), T);
    embedded_features = [embedded_features; points.'];
end;

epsilon = epsilon_estimation(embedded_features);
[phi, lambda] = diffusion_map2(embedded_features, 1);
points = embedding(phi, lambda, T);
save two_step_embedding.mat

plot_diffusion(phi, lambda, T, SPEECH_SEGMENTS_NUM);