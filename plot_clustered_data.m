function plot_clustered_data(data, class, varargin)
    colors = ['b' 'g' 'r' 'c' 'm' 'y' 'k'];
    plotchar = ['s', '+', 'x', 'o', '*', '^', 'v'];

    classes = unique(class);
    num_of_clusters = length(classes);
    
    if nargin >= 3
        h = varargin{1};
    else
        figure;
        h = gca();
    end;
    
     axes(h);
      
     for k=1:num_of_clusters
        points = data(class == classes(k), :);
        color = colors(mod(k, length(colors)) + 1);
        char = plotchar(mod(k, length(colors)) + 1);
        plot3(h, points(:,1), points(:,2), points(:,3), char, 'Color', color, 'MarkerSize', 5);
%         scatter3(h, points(:,1), points(:,2), points(:,3), 10, ones(size(points, 1), 1)*classes(k),  'filled');
        hold on;
    end;
%     scatter3(h, data(:,1), data(:,2), data(:,3), 10, class, 'filled');
    hold off;
    grid on;
end